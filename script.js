$(window).scroll(function(){

    var wScroll = $(this).scrollTop();
  
  
    if(wScroll > $('#projekty').offset().top - ($(window).height() / 1.8)) {
  
      $('figure').each(function(i){
  
        setTimeout(function(){
          $('figure').eq(i).addClass('is-showing');
        }, 250 * (i+1));
      });
  
    }
    if(wScroll > $('#kontakt').offset().top - ($(window).height() / 1.8)) {
  
      $('.anim div').each(function(i){
  
        setTimeout(function(){
          $('.anim div').eq(i).addClass('form-is-showing');
        }, 250 * (i+1));
      });
  
    }
  
  });
  
$(document).ready(function(){
    $('#cv').click(function() {
        $('#modal').css('display','block');
    })
    $('#close').click(function() {
        $('#modal').css('display','none');
    })   

})
